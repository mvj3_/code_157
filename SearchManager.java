String searchStr = "eoe";
/* 取得网页搜寻的intent */
Intent search = new Intent(Intent.ACTION_WEB_SEARCH);
search.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
search.putExtra(SearchManager.QUERY, searchStr);
 
final Bundle appData = getIntent().getBundleExtra(
        SearchManager.APP_DATA);
if (appData != null) {
    search.putExtra(SearchManager.APP_DATA, appData);
}
startActivity(search);